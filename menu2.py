
import sinks
import sources
import processors

# Función para mostrar el menú de fuente y solicitar la selección
def menu_fuente():
    fuentes_disponibles = ["load", "sin", "constant", "square"]
    fuente_seleccionada = None

    while fuente_seleccionada not in fuentes_disponibles:
        print("Dime una fuente (" + ", ".join(fuentes_disponibles) + "):")
        fuente_seleccionada = input()

    return fuente_seleccionada

# Función para solicitar los parámetros de la fuente seleccionada
def solicitar_parametros_fuente(fuente):
    print("Dame los parámetros para " + fuente + ".")

    if fuente == "sin":
        while True:
            try:
                nsample = int(input("nsamples: "))
                freq = int(input("freq: "))
                return nsample, freq

            except ValueError:
                print("Valor no valido")

    elif fuente == "square":
        while True:
            try:
                nsample = int(input("nsamples: "))
                level = int(input("level: "))
                return nsample, level

            except ValueError:
                print("Valor no valido")

    elif fuente == "constant":
        while True:

            try:
                nsample = int(input("nsamples: "))
                nperiod = float(input("nperiod: "))
                return nsample, nperiod

            except ValueError:
                print("Valor no valido")



# Función para mostrar el menú de sumidero y solicitar la selección
def menu_sumidero():
    sumideros_disponibles = ["play", "draw", "show", "info"]
    sumidero_seleccionado = None

    while sumidero_seleccionado not in sumideros_disponibles:
        print("Dime un sumidero (" + ", ".join(sumideros_disponibles) + "):")
        sumidero_seleccionado = input()

    return sumidero_seleccionado

# Función para solicitar los parámetros del sumidero seleccionado
def solicitar_parametros_sumidero(sumidero, fuente_seleccionada):
    print(f"Dame los parámetros para {sumidero} ")

    if sumidero == "play":
        sound = fuente_seleccionada
        return sound

    elif sumidero == "draw":
        while True:
            try:
                max_chars = int(input("max chars: "))
                return max_chars

            except ValueError:
                print("Valor no valido")


    elif sumidero == "show":
        while True:
            try:
                bool1 = (input("newline(True-False:"))
                newline = True if bool1.lower() == "true" else False
                return newline

            except ValueError:
                print("Valor no valido")

    elif sumidero == "info":
        sound = fuente_seleccionada
        return sound

#Función principal del programa
def main():
    fuente_seleccionada = menu_fuente()
    parametros_fuente = solicitar_parametros_fuente(fuente_seleccionada)


    sumidero_seleccionado = menu_sumidero()
    parametros_sumidero = solicitar_parametros_sumidero(sumidero_seleccionado, fuente_seleccionada)


    if fuente_seleccionada == "load":
        sound = sources.load(*parametros_fuente)
    elif fuente_seleccionada == "sin":
        sound = sources.sin(*parametros_fuente)

    elif fuente_seleccionada == "constant":
        sound = sources.constant(*parametros_fuente)

    elif fuente_seleccionada == "square":
        sound = sources.square(*parametros_fuente)


    # Ejecutar el sumidero con los parámetros indicados y el sonido resultante
    if sumidero_seleccionado == "play":
        sinks.play(sound)

    elif sumidero_seleccionado == "draw":
        sinks.draw(sound, parametros_sumidero)

    elif sumidero_seleccionado == "show":
        sinks.show(sound, parametros_sumidero)

    elif sumidero_seleccionado == "info":
        sinks.info(sound)


# Ejecutar la función principal
if __name__ == "__main__":
    main()