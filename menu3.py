
import sinks
import sources
import processors

# Función para mostrar el menú de fuente y solicitar la selección
def menu_fuente():
    fuentes_disponibles = ["load", "sin", "constant", "square"]
    fuente_seleccionada = None

    while fuente_seleccionada not in fuentes_disponibles:
        print("Dime una fuente (" + ", ".join(fuentes_disponibles) + "):")
        fuente_seleccionada = input()

    return fuente_seleccionada

# Función para solicitar los parámetros de la fuente seleccionada
def solicitar_parametros_fuente(fuente):
    print("Dame los parámetros para " + fuente + ".")

    if fuente == "sin":
        while True:
            try:
                nsample = int(input("nsamples: "))
                freq = int(input("freq: "))
                return nsample, freq

            except ValueError:
                print("Valor no valido")

    elif fuente == "square":
        while True:
            try:
                nsample = int(input("nsamples: "))
                level = int(input("level: "))
                return nsample, level

            except ValueError:
                print("Valor no valido")


    elif fuente == "constant":
        while True:
            try:
                nsample = int(input("nsamples: "))
                nperiod = float(input("nperiod: "))
                return nsample, nperiod

            except ValueError:
                print("Valor no valido")


#Funcion para mostrar el procesador y sus parametros
def menu_procesador():
    procesador_disponible = ["ampli", "shift", "trim", "repeat", "clean"]
    procesador_seleccionado = None

    while procesador_seleccionado not in procesador_disponible:
        print("Dime un procesador (" + ", ".join(procesador_disponible) + "):")
        procesador_seleccionado = input()

    return procesador_seleccionado

def ask_parametros_procesador(procesador, fuente1):
    print("Dame los parámetros para " + procesador + ".")
    if procesador == "ampli":
        while True:
            try:
                factor = int(input("Factor:"))
                return factor
            except ValueError:
                print("Valor no valido")

    elif procesador == "shift":
        while True:
            try:
                value = int(input("Valor:"))
                return value

            except ValueError:
                print("Valor no valido")

    elif procesador =="trim":
        while True:
            try:
                reduction = int(input("Reduccion: "))
                start = bool(input("Star(True-False):"))
                return reduction, start
            except ValueError:
                print("Valor no valido")

    elif procesador == "repeat":
        while True:
            try:
                factor = int(input("Factor: "))
                return factor
            except ValueError:
                print("Valor no valido")

    elif procesador == "clean":
        while True:
            try:
                level = (input("Level: "))
                return level
            except ValueError:
                print("Valor no valido")

# Función para mostrar el menú de sumidero y solicitar la selección
def menu_sumidero():
    sumideros_disponibles = ["play", "draw", "show", "info"]
    sumidero_seleccionado = None

    while sumidero_seleccionado not in sumideros_disponibles:
        print("Dime un sumidero (" + ", ".join(sumideros_disponibles) + "):")
        sumidero_seleccionado = input()

    return sumidero_seleccionado

# Función para solicitar los parámetros del sumidero seleccionado
def solicitar_parametros_sumidero(sumidero, fuente_seleccionada):
    print("Dame los parámetros para " + sumidero + ".")

    if sumidero == "play":
        sound = fuente_seleccionada
        return sound
    elif sumidero == "draw":
        while True:
            try:
                max_chars = int(input("max chars: "))
                return  max_chars

            except ValueError:
                print("Valor no valido")

    elif sumidero == "show":
        while True:
            try:
                bool1 = (input("newline(True-False:"))
                newline = True if bool1.lower() == "true" else False
                return newline

            except ValueError:
                print("Valor no valido")

    elif sumidero == "info":
        sound = fuente_seleccionada
        return sound

#Función principal del programa
def main():
    fuente_seleccionada = menu_fuente()
    parametros_fuente = solicitar_parametros_fuente(fuente_seleccionada)
    fuente1 = fuente_seleccionada, parametros_fuente                                    #--> Fuente original

    procesador_seleccionado = menu_procesador()
    parametros_procesador = ask_parametros_procesador(procesador_seleccionado, fuente1)

    fuente2 = (fuente1, parametros_procesador)                                          #--> Fuente tras pasar por el procesador
    sumidero_seleccionado = menu_sumidero()
    parametros_sumidero = solicitar_parametros_sumidero(sumidero_seleccionado, fuente2)

## Debido a que la fuente original sera pasada por un procesador, sera necesario crear otra fuente con los valores nuevos de la fuente tras pasar por el procesador
## para poder aplicarle el sumidero a esa nueva fuente, y no a la fuente original

    if fuente_seleccionada == "load":
         fuente1 = sources.load(*parametros_fuente)
    elif fuente_seleccionada == "sin":
         fuente1=sources.sin(*parametros_fuente)

    elif fuente_seleccionada == "constant":
         fuente1 = sources.constant(*parametros_fuente)

    elif fuente_seleccionada == "square":
         fuente1 = sources.square(*parametros_fuente)

    ##Ejecucion del procesador
    if  procesador_seleccionado == "ampli":
        fuente2 = processors.ampli(fuente1, parametros_procesador)

    elif  procesador_seleccionado == "shift":
        fuente2 = processors.shift(fuente1, parametros_procesador)

    elif  procesador_seleccionado == "trim":
        fuente2= processors.trim(fuente1, parametros_procesador)

    elif  procesador_seleccionado == "repeat":
        fuente2 = processors.repeat(fuente1,parametros_procesador)

    elif procesador_seleccionado == "clean":
        fuente2 = processors.clean(fuente1, parametros_procesador)

    # Ejecutar el sumidero con los parámetros indicados y el sonido resultante
    if sumidero_seleccionado == "play":
        sinks.play(fuente2)

    elif sumidero_seleccionado == "draw":
        sinks.draw(fuente2, parametros_sumidero)

    elif sumidero_seleccionado == "show":
        sinks.show(fuente2, parametros_sumidero)

    elif sumidero_seleccionado == "info":
        sinks.info(fuente2)


# Ejecutar la función principal
if __name__ == "__main__":
    main()