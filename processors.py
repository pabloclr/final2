"""Methods for processing sound (sound processors)"""

import math

import soundfile

import config

def ampli(sound, factor: float):
    """Amplify signal (sound) by the specified factor

    :param sound:  sound (list of samples) to amplify
    :param factor: amplification factor
    :return:     list of samples (sound)
    """
    out_sound = [0] * len(sound)
    for nsample in range(len(sound)):
        value = sound[nsample] * factor
        if value > config.max_amp:
            value = config.max_amp

        elif value < - config.max_amp:
            value = - config.max_amp

        out_sound[nsample] = int(value)
    return out_sound


def shift(sound, value: int):
    """Desplazar las muestras del sonido por un valor

        :param sound:     lista de muestras (sound)
        :param value: valor de desplazamiento
        :return:          sonido desplazado
        """
    shift_sound = [0] * (len(sound) + value)                #--> Agregamos el valor de value para que la longitud del sonido aumente tantas veces como indique valor
    for indice_original, nsample in enumerate(sound):       #--> Conseguimos le indice (indice_original) como el valor(nsample) de sound

            if value > config.max_amp:
                nsample = config.max_amp

            elif value < -config.max_amp:
                nsample = -config.max_amp

            shifted_index = indice_original + value         #-->Creamos el nuevo indice con el valor introducido
            if shifted_index < len(shift_sound) :            #--> Para que no se genere una lista con longitud mayor a la longitud del sonido
                shift_sound[shifted_index]= nsample            #--> Asignamos el valor de nsample a la posicion ya movida por el valor

    return shift_sound

def trim(sound, reduction: int, start: bool):
    """Recortar un determinado numero de muestras del principio o el final de la señal sonido

    :param sound:     lista de muestras (sound)
    :param reduction: numero de muestrar a reducir
    :param start:     booleano inidicando si tiene que quitar muestras del principio True o del final False
    :return:          sonido recortado
    """

    trimmed_sound = [0] * len(sound)                    #--> Creamos una lista de 0 con las longitud de sound
    if reduction >= len(sound):                         #--> Si el valor de reduccion es mayor a la longitud del sonido
        return []                                       #-->Devolvemos una lista vacia

    if start:                                           #--> Si la variable start es True:
        trimmed_sound = sound[reduction:]               #--> Se eliminan elementos del principio de la lista
    else:                                               #--> Si es False
        trimmed_sound = sound[:-reduction]              #--> Se eliminan de la parte final de la lista

    return trimmed_sound



def repeat(sound, factor: int):
    """Repetir la señal sonido un numero determinado de veces

    :param sound:  lista de muestras (sound)
    :param factor: factor de repeticion (veces que se repite el sonido
    :return:       sonido repetido
    """
    repeat_sound = [0] * len(sound)
    if factor <= 0:
        return []

    repeat_sound = sound + (sound * factor)         #--> Concatenamos el sonido original con si mismo multiplicado por el factor

    return repeat_sound                             #--> Devolvemos el sonido repetido

def clean(sound, level: int):
    """Limpia aquellas muestras que sean menores a el valor (level) introducido

        :param sound:  lista de muestras (sound)
        :param level:   valor limite de muestras que se quieren limpiar
        :return:       sonido limpiado
    """
    clean_sound = []                        #-->Creamos una lista
    for nsample in sound:                   #--> Para nsamples en sonido
        if nsample > level:                 #--> Si nsample es mayor al valor establecido
            clean_sound.append(nsample)     #--> se agrega en la lista el valor de nsample

        elif nsample <= level:              #--> Si es menor
            clean_sound.append(0)           #--> Se establece su valor en 0

    return clean_sound

def round(sound):
    """Redondear el sonido calculado para 3 muestras
        :param sound:  lista de muestras (sound)
        """
    round_sound = []                                                                #--> Creamos una lista
    sample = 4                                                                      #--> Establecemos el valor de la muestra
    for samples in range (len(sound)):                                              #--> Si el valor introducido se encuentra en la longitud de sound
        media = (sound[sample-1] + sound[sample] + sound[sample +1]) /3             #--> Se realiza la media de la muestra anterior, la muestra siguiente, y la seleccionada
        round_sound = media

    print(f"El valor medio de la muestra  {sample} es: {round_sound}")


