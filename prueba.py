#!/usr/bin/env python3

import sources
import sinks
import processors
def main ():
    sound = sources.sin(4,5)
    sound2 = processors.clean(sound,30)
    sinks.show(sound2, False)


if __name__ == '__main__':
    main()