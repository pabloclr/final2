"""Methods for storing sound, or playing it"""

import array
import config

import sounddevice


def play(sound):
    sound_array = array.array('h', [0] * len(sound))
    for nsample in range(len(sound)):
        sound_array[nsample] = sound[nsample]
    sounddevice.play(sound_array, config.samples_second)
    sounddevice.wait()

def draw(sound, max_chars: int):
    total_chars = max_chars * 2
    for nsample in range(len(sound)):
        if sound[nsample] > 0:
            print(' ' * max_chars, end='')
            nchars = int(max_chars * (sound[nsample] / config.max_amp))
            print('*' * nchars)
        else:
            nchars = int(max_chars * (-sound[nsample] / config.max_amp))
            print(' ' * (max_chars - nchars), end='')
            print('*' * nchars)

def show(sound, newline:bool):
    """Print the samples of a sound signal
    """
    if newline == True :                        #--> Para newline: True, cada muestra aparecera en una linea utilizando el comportamiento predeterminado de print()
        for sample in sound:
            print(sample)
    else:                                       #--> Para newline: False, cada muestra aparecera seguido de una coma
        for i, sample in enumerate(sound):      #--> Iteramos sobre los elementos de sound, cada iteracion i corresponde con el indice de sample
            if i == len(sound) - 1:             #--> Observamos si estamos en la ultima iteracion y no necesitamos escribir una coma al final.
                print(sample, end='')
            else:
                print(sample, end=', ')         #-->Si no es el ultimo elemento, se imprime una coma para separar los valores

def info(sound):
    n_samples = len(sound)
    print(f"Samples: {n_samples}")

    amp_max = max(sound)                                      #--> Amplitud maxima en sound
    Elem_max = sound.index(max(sound))    #--> (n_samples-1): ultimo elemento de sound; sound[::-1].index(max_val): indice de la amplitud maxima; Resta total: Indice de la amplitud maxima en sound
    print(f"Max value: {amp_max} (sample: {Elem_max})")

    amp_min = min(sound)    #--> Amplitud minima en sound
    Elem_min= sound.index(min(sound))
    print(f"Min value: {amp_min} (sample: {Elem_min})")

    mean =sum(sound) / len(sound)
    print(f"Mean value: {mean}")

    n_positive = sum(1 for sample in sound if sample > 0)   #--> Para valores positivos de sound
    print(f"Positive samples: {n_positive}")
    n_negative = sum(1 for sample in sound if sample < 0)   #--> Para los valores negativos de sound
    print(f"Negative samples: {n_negative}")
    n_zero = sum(1 for sample in sound if sample == 0)      #--> Para los valores = 0 en sound
    print(f"Null samples: {n_zero}")
