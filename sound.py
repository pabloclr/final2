import sys
import sources
import processors
import sinks


def parse_args():
    if len(sys.argv) < 7:
        print(f"Usage: {sys.argv[0]}<source> <process> <factor> <factor2> <sink> <var>")
        exit(0)

    #Asignamos el argumento a los valores pasados por el programa
    source = sys.argv[1]
    nsamples = int(sys.argv[2])
    freq = int(sys.argv[3])
    process = sys.argv[4]
    factor = int(sys.argv[5])
    start = None
    sink = sys.argv[6]
    if sink == "info" or sinks == "play":   #--> Como tanto info como play son los unicos que tienen un parametro, lo asignamos a 0 para que no cuente
        var = [0]
    else:
        var = sys.argv[7:]                  #--> En caso contrario los valores pasados despues del sumidero seran los parametros de las funciones correspondientes

    if process == "trim":                   #--> Al igual que info y play, trim  utiliza 3 parametros, por lo que en el caso de escribir trim, se asignaran los valores de la siguiente manera
        start = bool(sys.argv[6])
        sink = sys.argv[7]
        var = sys.argv[8:]


    return source, nsamples, freq, process, factor,start ,  sink, var

def main():
    source, nsamples, freq, process, factor, start, sink, var = parse_args()

    if source == 'sin':
        sound = sources.sin(nsamples, freq)                         #--> Con import sources, conseguimos utilizar las respectivas funciones
    elif source == 'constant':
        sound= sources.constant(nsamples, freq)
    elif source == 'square':
        sound = sources.square(nsamples, freq)

    else:
        sys.exit("Unknown source")

    if process =='amp':
        sound2= processors.ampli(sound, factor)                     #--> Con import process, conseguimos utilizar las respectivas funciones
    elif process == 'shift':
        sound2 = processors.shift(sound, factor)
    elif process == 'trim':
        sound2 = processors.trim(sound, factor, start)
    elif process == 'repeat':
        sound2 = processors.repeat(sound, factor)

    else:
        sys.exit("unknown process")

    if sink == "play":
        sinks.play(sound2)
    elif sink == "draw":
        var1 = int(var[0])
        sinks.draw(sound2, var1)
    elif sink == "show":
        newline = True if var[0].lower() == "true" else False
        sinks.show(sound2,newline)
    elif sink == "info":
        sinks.info(sound2)
    else:
        sys.exit("Uknown sink")

if __name__ == '__main__':
    main()



