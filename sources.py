"""Methods for producing sound (sound sources)"""

import math

import soundfile

import config

def load(path: str):
    """Load sound from a file

    :param path: path of the file to read
    :return:     list of samples (sound)
    """

    data, fs = soundfile.read(path, dtype='int16')

    # Create q liwt of integers
    sound = [0] * len(data)
    # Convert list of int16 to list of int
    for nsample in range(len(data)):
        sound[nsample] = data[nsample][0]

    return sound

def sin(nsamples: int, freq: float):
    """Produce a list of samples of a sinusoidal signal (sound)

    :param nsamples: number of samples
    :param freq:     frequency of the signal, in Hz (cycles/sec)
    :return:         list of samples (sound)
    """

    # Create q list of integers
    sound = [0] * nsamples

    # Compute sin samples (maximum amplitude, freq frequency)
    for nsample in range(nsamples):
        t = nsample / config.samples_second
        sound[nsample] = int(config.max_amp * math.sin(2 * config.pi * freq * t))

    return sound

def constant( nsamples: int, level: int):
    """Produce a list of samples of a sinusoidal signal (sound)

      :param nsamples: number of samples
      :param level:     sample value
      :return:         list of samples (sound)
      """

    sound = [0] * nsamples                      #--> Creamos una lista con valores de 0 para nsamples elementos(introducidos por el usuario)
    for nsamples in range(len(sound)):          #--> Iteramos sobre los indices de la lista sound
        sound[nsamples] = level                 #--> Como queremos una señal constante, la cual todos sus valores son iguales, asignamos el valor de level a la posicion de nsample de la lista sound

        if abs(level) > config.max_amp:         #--> Devolvemos el valor absoluto y si es mayor a la maxima amplitud
            sound[nsamples] = config.max_amp    #--> Las muestras valdran la maxima amplitud

        elif abs(level) < config.max_amp:       #--> Devolvemos el valor absoluto de level y si es menor a la minima amplitud
            sound[nsamples] = -config.max_amp   #--> Las muestras valdran la minima amplitud

    return sound




def square (nsamples : int, nperiod :int):
    """Produce a list of samples of a sinusoidal signal (sound)

      :param nsamples: number of samples
      :param nperiod:   number of samples in a period
      :return:         list of samples (sound)
    """

    # Creamos la lista de enteros
    sound = [0] * nsamples

    for nsamples in range(len(sound)):          #--> Iteramos sobre los indices de la lista sound
        periodo = nsamples % nperiod            #--> Definimos periodo, y calculamos el resto(para obtener un resultado entero) entre el numero de muestras y el numero de muestras en un periodo obteniendo el valor del periodo actual
                                                #--> Una señal cuadrada, por cada periodo, tendremos una parte positiva en la mitad  de las muestras, y otra negativa en la otra mitad
        if periodo <= ((nperiod // 2)-1):       #-->con (nperiod //2), obtenemos el cociente de la division entera, obteniendo la mitad de nperiod, utilizando el -1 se ajusta el indice para las muestras negativas
            sound[nsamples] = config.max_amp
        else:
            sound[nsamples] = -config.max_amp

    return sound





